/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Components project.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "adeclarativeindicatorcontainer.h"
#if defined(Q_OS_SYMBIAN) && defined(HAVE_SYMBIAN_INTERNAL)
#include "adeclarativeindicatordatahandler.h"
#endif // Q_OS_SYMBIAN && HAVE_SYMBIAN_INTERNAL

class ADeclarativeIndicatorContainerPrivate
{
public:
    ADeclarativeIndicatorContainerPrivate();
    ~ADeclarativeIndicatorContainerPrivate();

#if defined(Q_OS_SYMBIAN) && defined(HAVE_SYMBIAN_INTERNAL)
    CADeclarativeIndicatorDataHandler *dataHandler;
#else
    int *dataHandler; // dummy
#endif // Q_OS_SYMBIAN && HAVE_SYMBIAN_INTERNAL

    QColor indicatorColor;
    QSize indicatorSize;
    int indicatorPadding;
    int maxIndicatorCount;
    bool layoutRequestPending;

};

ADeclarativeIndicatorContainerPrivate::ADeclarativeIndicatorContainerPrivate()
    : dataHandler(0), layoutRequestPending(false)
{
}

ADeclarativeIndicatorContainerPrivate::~ADeclarativeIndicatorContainerPrivate()
{
    delete dataHandler;
}


ADeclarativeIndicatorContainer::ADeclarativeIndicatorContainer(QDeclarativeItem *parent)
    : QDeclarativeItem(parent), d_ptr(new ADeclarativeIndicatorContainerPrivate)
{
#if defined(Q_OS_SYMBIAN) && defined(HAVE_SYMBIAN_INTERNAL)
    QT_TRAP_THROWING(d_ptr->dataHandler = CADeclarativeIndicatorDataHandler::NewL(this));
#endif // Q_OS_SYMBIAN && HAVE_SYMBIAN_INTERNAL

    connect(this, SIGNAL(indicatorSizeChanged()), this, SLOT(layoutChildren()));
    connect(this, SIGNAL(indicatorPaddingChanged(int)), this, SLOT(layoutChildren()));
    connect(this, SIGNAL(maxIndicatorCountChanged(int)), this, SLOT(layoutChildren()));
}

ADeclarativeIndicatorContainer::~ADeclarativeIndicatorContainer()
{
}

QColor ADeclarativeIndicatorContainer::indicatorColor() const
{
    Q_D(const ADeclarativeIndicatorContainer);
    return d->indicatorColor;
}

void ADeclarativeIndicatorContainer::setIndicatorColor(const QColor &color)
{
    Q_D(ADeclarativeIndicatorContainer);
    if (d->indicatorColor != color) {
        d->indicatorColor = color;
        emit indicatorColorChanged(d->indicatorColor);
    }
}

int ADeclarativeIndicatorContainer::indicatorWidth() const
{
    Q_D(const ADeclarativeIndicatorContainer);
    return d->indicatorSize.width();
}

void ADeclarativeIndicatorContainer::setIndicatorWidth(int width)
{
    Q_D(ADeclarativeIndicatorContainer);
    if (d->indicatorSize.width() != width) {
        d->indicatorSize.setWidth(width);
        emit indicatorSizeChanged();
    }
}

int ADeclarativeIndicatorContainer::indicatorHeight() const
{
    Q_D(const ADeclarativeIndicatorContainer);
    return d->indicatorSize.height();
}

void ADeclarativeIndicatorContainer::setIndicatorHeight(int height)
{
    Q_D(ADeclarativeIndicatorContainer);
    if (d->indicatorSize.height() != height) {
        d->indicatorSize.setHeight(height);
        emit indicatorSizeChanged();
    }
}

int ADeclarativeIndicatorContainer::indicatorPadding() const
{
    Q_D(const ADeclarativeIndicatorContainer);
    return d->indicatorPadding;
}

void ADeclarativeIndicatorContainer::setIndicatorPadding(int padding)
{
    Q_D(ADeclarativeIndicatorContainer);
    if (d->indicatorPadding != padding) {
        d->indicatorPadding = padding;
        emit indicatorPaddingChanged(d->indicatorPadding);
    }
}

int ADeclarativeIndicatorContainer::maxIndicatorCount() const
{
    Q_D(const ADeclarativeIndicatorContainer);
    return d->maxIndicatorCount;
}

void ADeclarativeIndicatorContainer::setMaxIndicatorCount(int maxCount)
{
    Q_D(ADeclarativeIndicatorContainer);
    if (d->maxIndicatorCount != maxCount) {
        d->maxIndicatorCount = maxCount;
        emit maxIndicatorCountChanged(d->maxIndicatorCount);
    }
}

void ADeclarativeIndicatorContainer::layoutChildren()
{
    Q_D(ADeclarativeIndicatorContainer);
    if (!d->layoutRequestPending) {
        d->layoutRequestPending = true;
        QMetaObject::invokeMethod(this, "doLayoutChildren", Qt::QueuedConnection);
    }
}

void ADeclarativeIndicatorContainer::doLayoutChildren()
{
    Q_D(ADeclarativeIndicatorContainer);

    int xPosition = 0;
    int itemsShown = 0;
    const QSize itemSize(d->indicatorSize);

    for (int i = 0; i < childItems().count(); i++) {
        QDeclarativeItem *child = qobject_cast<QDeclarativeItem *>(childItems().at(i)->toGraphicsObject());
        if (child && child->isVisible()) {
            if (itemsShown >= d->maxIndicatorCount && d->maxIndicatorCount >= 0) {
                child->setSize(QSize(0, 0));
                continue;
            }

            if (itemsShown++)
                xPosition += d->indicatorPadding;

            child->setPos(xPosition, 0);
            child->setSize(itemSize);

            xPosition += child->width();
        }
    }

    setImplicitWidth(xPosition);
    setImplicitHeight(itemSize.height());
    d->layoutRequestPending = false;
}

